function [] = proj1(v1, v2)
    if (isempty(v1) & isempty(v2))
        v1 = input('Enter vector 1: ');
        v2 = input('Enter vector 2: ');
    endif
    
    if !(isempty(v1) | isempty(v2))
        if (isequal(v1, v2));
            fprintf('Vectors are equal.');
        else 
            fprintf('Vectors are not equal.');
        endif
    else
        fprintf('Vectors cannot be empty.');
    endif
end